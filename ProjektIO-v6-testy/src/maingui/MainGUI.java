package maingui;

import course.ChangeCoursePanel;
import course.Course;
import course.DrawCourse;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import static maingui.MainGUI.*;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import startpackage.StartGUI;

/**
 *
 * @author Daniel
 */
public class MainGUI
{
    private JFrame frame ;
    private static final String FINAL_FILE_WITH_COURSES = "courses.txt";
    private ArrayList<Course> coursesList;
    private static final int FRAME_WIDTH = 1300;
    private static final int FRAME_HEIGHT = 730;
    private static final int FIRST_COLUMN_WIDTH = 1300/(7*2);
    private static final int REST_COLUMN_WIDTH = 1300/6;
    private static final int ROW_HEIGHT = 40;
    private static final int TABLE_WIDTH = 1080;
    private static final int TABLE_HEIGHT = 520;


    private tablePane panel;
    private InvisiblePanel invisiblePanel ;
    private String tableTitle;
    private String viewType;
    private String objectViewType;

    
     public MainGUI(String tableTitle , String vt , ArrayList<Course> list)
     {
        this.tableTitle = tableTitle;
        this.viewType = vt;
        coursesList= new ArrayList<>();
        frame = new JFrame();
        
        coursesList = list;
        
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        
        if(vt.contains("Student")) //tworzenie odpowiedniego widoku w zaleznosci od wyboru
            initStudentsGUI();
        
        frame.setVisible(true);
     }
    
    public MainGUI(String tableTitle , String vt , ArrayList<Course> list , String objectVW)
    {
        this(tableTitle , vt , list);
        objectViewType = objectVW;
        
        if(vt.contains("Nauczyciel")) //tworzenie odpowiedniego widoku w zaleznosci od wyboru
            initTeachersGUI();

        if(vt.contains("Sala")) //tworzenie odpowiedniego widoku w zaleznosci od wyboru
            initHallsGUI();
    }

    
    
    public void writeToFile() throws IOException {
	File fout = new File(FINAL_FILE_WITH_COURSES);
	FileOutputStream fos = new FileOutputStream(fout);
 
	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
 
	for (int i=0;i<coursesList.size();i++) 
        {
		bw.write(coursesList.get(i).getName() + ",");
                bw.write(coursesList.get(i).getWhere()+ ",");
                bw.write(coursesList.get(i).getCourseInstructor()+ ",");
                bw.write(coursesList.get(i).getDay() + ",");
                bw.write(coursesList.get(i).getTimeStart()+ ",");
                bw.write(coursesList.get(i).getDuration()+ ",");
                
                if(i!=coursesList.size()-1)
                    bw.write(coursesList.get(i).getCoursesType()+ ",");
                else
                    bw.write(coursesList.get(i).getCoursesType());
                
		bw.newLine();
	}
 
	bw.close();
}

    private void initStudentsGUI() 
    {
        frame.setTitle("Rozkład zajęć");
        
        invisiblePanel = new InvisiblePanel(coursesList , this);
        
        panel = new tablePane(null , tableTitle,true);
        
        frame.add(new buttonFrame(this));
        frame.add(invisiblePanel);
        frame.add(panel);

    }
    
    public ArrayList<Course> getCoursesList() {
        return coursesList;
    }
    
    public static int getFRAME_WIDTH() {
        return FRAME_WIDTH;
    }

    public static int getFRAME_HEIGHT() {
        return FRAME_HEIGHT;
    }
    

    public static int getWIDTH() {
        return FRAME_WIDTH;
    }

    public static int getHEIGHT() {
        return FRAME_HEIGHT;
    }
    
    public static int getFIRST_COLUMN_WIDTH() {
        return FIRST_COLUMN_WIDTH;
    }

    public static int getREST_COLUMN_WIDTH() {
        return REST_COLUMN_WIDTH;
    }

    public static int getROW_HEIGHT() {
        return ROW_HEIGHT;
    }
    
    public static int getTABLE_WIDTH() {
        return TABLE_WIDTH;
    }

    public static int getTABLE_HEIGHT() {
        return TABLE_HEIGHT;
    } 
    
    public JFrame getFrame() {
        return frame;
    }
    
    public String getTableTitle() {
        return tableTitle;
    }

    public void setTableTitle(String tableTitle) {
        this.tableTitle = tableTitle;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }
    
    public String getObjectViewType() {
        return objectViewType;
    }

    public void setObjectViewType(String objectViewType) {
        this.objectViewType = objectViewType;
    }

    private void initTeachersGUI() {
        frame.setTitle("Rozkład zajęć dla nauczyciela");
        
        invisiblePanel = new InvisiblePanel(coursesList , this);
        panel = new tablePane(null , tableTitle,false);
        
        frame.add(new buttonFrame(this));
        frame.add(invisiblePanel);
        frame.add(panel);
    }

    private void initHallsGUI() {
        frame.setTitle("Godziny zajęcia sali");
        
        invisiblePanel = new InvisiblePanel(coursesList , this);
        panel = new tablePane(null , tableTitle , false);
        
        frame.add(new buttonFrame(this));
        frame.add(invisiblePanel);
        frame.add(panel);
    }


}

class tablePane extends JPanel
{
    private ArrayList<String> tableTitleHours = new ArrayList<>();
    private ArrayList<String> tableTitles = new ArrayList<>();
    String tableTitle;
    boolean isStudentSchedule;
    
    public tablePane(LayoutManager lm , String tableTitle , boolean isStudentSchedule) 
    {
        super(lm);
        setBounds(0,0,WIDTH,HEIGHT);
        this.tableTitle = tableTitle;
        initTableTitles();
        this.isStudentSchedule = isStudentSchedule;
    }

    
    
    
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        draw(g);
    }
    
    public void draw(Graphics g) 
    {
        
        Graphics2D g2 = (Graphics2D) g;
        
        float thickness = 1.5f;
        Stroke oldStroke = g2.getStroke();
        g2.setStroke(new BasicStroke(thickness));
        g.setColor(Color.black);
        
        //rysujemy tabele przy zadanych warunkach rect(x , y , width , height)
         
        //rysujemy nagłówki tabeli(pionowo - godziny)
        for(int i=50 , k=0; i<MainGUI.getHEIGHT() && k<14 ; i+=40 , k++)
        {
                g2.drawRect(0,i,getFIRST_COLUMN_WIDTH(), 40);
        }
        //rysujemy nagłówki poziomo (od - do oraz dni tygodnia )
            for(int j=getFIRST_COLUMN_WIDTH(); j< MainGUI.getWIDTH()-300; j+=getREST_COLUMN_WIDTH())
                g2.drawRect(j,50,getREST_COLUMN_WIDTH(), 40);
        
        for(int i=90 , k=0; i<MainGUI.getHEIGHT() && k<12 * 4 + 1 ;k++ , i+=10)
            for(int j=getFIRST_COLUMN_WIDTH() ; j< MainGUI.getWIDTH()-300; j+=getREST_COLUMN_WIDTH())
            {
                 g2.drawRect(j,i,getREST_COLUMN_WIDTH(), 40);
            }
     
        if(isStudentSchedule){
        // rysujemy uwagi do planu zajec
        g2.setFont(new Font("TimesRoman", Font.BOLD, 25));
        g2.drawString("Legenda", MainGUI.getWIDTH() - 120, 100);
        
        //wyklad
        g2.setFont(new Font("TimesRoman", Font.PLAIN, 10));
        g2.setColor(new Color(146,255,183));
        g2.fillRect(MainGUI.getWIDTH() - 120,130,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("wykład" ,MainGUI.getWIDTH() - 70, 145 );
        
        //cwiczenia
        g2.setColor(new Color(238,175,30));
        g2.fillRect(MainGUI.getWIDTH() - 120,160,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("cwiczenia" ,MainGUI.getWIDTH() - 70, 175 );
        
        //lab
        g2.setColor(new Color(168,243,218));
        g2.fillRect(MainGUI.getWIDTH() - 120,190,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("laboratorium" ,MainGUI.getWIDTH() - 70, 205 ); 
        
        //projekt
        g2.setColor(Color.yellow);
        g2.fillRect(MainGUI.getWIDTH() - 120,220,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("projekt" ,MainGUI.getWIDTH() - 70, 235 ); 
        
        //seminarium
        g2.setColor(new Color(208,117,202));
        g2.fillRect(MainGUI.getWIDTH() - 120,250,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("seminarium" ,MainGUI.getWIDTH() - 70, 265 ); 
        
        }
        
        g2.setStroke(oldStroke);
        
        fillTableHeaders(g2);
        

    }
    
    private void fillTableHeaders(Graphics2D g2)
    {
        //tytuł czerwony
        g2.setColor(Color.red);
        g2.setFont(new Font("TimesRoman", Font.BOLD, 25));
        g2.drawString(tableTitles.get(0), (MainGUI.getWIDTH()/2) - 350, 25);
        
        g2.setColor(Color.black);
        g2.setFont(new Font("TimesRoman", Font.PLAIN, 20));
        
        //wpisywanie do nagłówkow godzin odpowiednich stringow (pionowo godziny)
        for(int i=100 , k=0;k<tableTitleHours.size() ; i+=40 , k++)
        {
            g2.drawString(tableTitleHours.get(k), 15, i+25);
        }
        
        g2.drawString(tableTitles.get(1), 10, 85);
        
        for(int i=MainGUI.getWIDTH()/6 - 50 , k=2; k<tableTitles.size(); k++  , i+=MainGUI.getWIDTH()/6)
        {
            g2.drawString(tableTitles.get(k), i, 80);
        }
        
    }

    private void initTableTitles() {
       int k;
       for(int i=8;i<=20;i++){
           k=i+1;
           tableTitleHours.add(i + "-" + k + "");
       }
       
       tableTitles.add(tableTitle);
       tableTitles.add("od - do");
       tableTitles.add("Poniedziałek");
       tableTitles.add("Wtorek");
       tableTitles.add("Środa");
       tableTitles.add("Czwartek");
       tableTitles.add("Piątek");
           
    }
    

    
}

    class InvisiblePanel extends JPanel {
        private ArrayList<DrawCourse> drawCourse = new ArrayList<>();
        private MainGUI MainGUI;

    public MainGUI getMainGUI() {
        return MainGUI;
    }
        private MouseAdapter mouseListener =
        new MouseAdapter() {
            
            int pozx,pozy;
            

            @Override
            public void mousePressed(MouseEvent me) {

                  if (me.getClickCount() == 2) 
                  {
                      System.out.println("podwojne klikniecie na oknie");
                      for(DrawCourse c : drawCourse)
                          if(c.getRect().contains(me.getX(),me.getY()))
                          {
                            System.out.println("Otwierany panel zmiany godziny");
                            ChangeCoursePanel panel = new ChangeCoursePanel(c,(int)c.getP().getX() , (int)c.getP().getY() , getMainGUI());
                          }
                  }
  
                
            for(DrawCourse c : drawCourse)
               if(c.getRect().contains(me.getX(),me.getY())){
                   c.setFocus(true);
                   Point p = new Point((int)c.getP().getX() , (int)c.getP().getY());
                   c.setPointLastReleased(p);
                   repaint();
                }
            
            for(DrawCourse c : drawCourse)
               if(c.isFocus())
               {
                   pozx = c.getP().x;
                   pozy = c.getP().y;
                   repaint();
               }
                    
            }

            @Override
            public void mouseReleased(MouseEvent e) {
               
            for(DrawCourse c : drawCourse)
               if(c.isFocus())
                {
                    
                   System.out.println("Zmiana polozenia obiektu");
                   Rectangle rect = c.getRect();
                   double y = rect.getY();
                   rect.setLocation(c.getXBeforeMoveObject(e.getX() , c), (int)y);
                   
                 for(DrawCourse c1 : drawCourse)
                    if((c1.getCourse().compareTo(c.getCourse())) != 0)
                        if(rect.intersects(c1.getRect()))
                        {
                            System.out.println("Kolizja");
                            JOptionPane.showMessageDialog(MainGUI.getFrame(),"Kolizja podczas zmiany struktury planu zajęć !","Błąd",JOptionPane.ERROR_MESSAGE);
                            c.getP().setLocation(c.getPointLastReleased());
                            c.setToRightColumn(c.getPointLastReleased().x);
                            c.setToRightRow(c.getPointLastReleased().y);
                            c.setFocus(false);
                            c.checkIfChanged();
                            repaint();
                            return;
                        }
                        
                        
                  c.setToRightColumn(e.getX());
                  c.setToRightRow((int)c.getP().getY());
                  c.setFocus(false);
                  c.checkIfChanged();
                  repaint();
                    try {
                        MainGUI.writeToFile();
                    } catch (IOException ex) {
                        System.out.println("Błąd podczas zapisu do pliku");
                        JOptionPane.showMessageDialog(MainGUI.getFrame(),"Blad podczas zapisu do pliku , nie moge kontunuowac , exit ","Błąd",JOptionPane.ERROR_MESSAGE);
                        System.out.println("Blad podczas zapisu do pliku , nie moge kontunuowac , exit ");
                        System.exit(0);
                    }
                }
            repaint();
            }
            @Override
            public void mouseMoved(MouseEvent me) {
            }

            @Override
            public void mouseDragged(MouseEvent me) {

            for(DrawCourse c : drawCourse)
               if(c.isFocus() && c.changeLocation(me.getX() - c.getObjectWidth()/2,me.getY() - c.getObjectHeight()/2))
               {
                   repaint();                 
               }                  
            }
        };
       
        



        public InvisiblePanel(ArrayList<Course> courseArray , MainGUI mainGui) {
            MainGUI = mainGui;
            // drawing should be in blue
            setForeground(Color.blue);
            // background should be black, except it's not opaque, so 
            // background will not be drawn
            setBackground(Color.black);
            // set opaque to false - background not drawn
            setOpaque(false);
            setBounds(0, 0, MainGUI.getWIDTH(),MainGUI.getHEIGHT());
            
            if(MainGUI.getViewType().contains("Student"))   //jesli widok studenta to mozliwa jest edycja planu zajec
            {
                addMouseListener(mouseListener);
                addMouseMotionListener(mouseListener);                
            }

            
            if(MainGUI.getViewType().contains("Student"))
                for(Course c : courseArray)
                    drawCourse.add(new DrawCourse(c));
            else if(MainGUI.getViewType().contains("Nauczyciel"))
            {
                for(Course c : courseArray)
                    if(c.getCourseInstructor().contains(MainGUI.getObjectViewType()))
                        drawCourse.add(new DrawCourse(c ,MainGUI.getObjectViewType() ));
            } 
            else if(MainGUI.getViewType().contains("Sala"))
            {
                for(Course c : courseArray)
                    if(c.getWhere().contains(MainGUI.getObjectViewType()))
                        drawCourse.add(new DrawCourse(c,MainGUI.getObjectViewType() ));                
            }
       
        }

        // This is Swing, so override paint*Component* - not paint
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            
            Graphics2D g2 = (Graphics2D) g;
            
                for(DrawCourse c : drawCourse)
                    c.draw(g2);
   
        }
        
    public ArrayList<DrawCourse> getDrawCourse() {
        return drawCourse;
    }


        
       
        
    }

    class buttonFrame extends JPanel{

        public buttonFrame(MainGUI maingui) 
        {
            JButton b = new JButton("<--- POWRÓT");
            b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                System.out.println("Przycisk POWROT klikniety ");
                    maingui.getFrame().setVisible(false);
                    maingui.getFrame().dispose();
                    new StartGUI();
                }
            });
            add(b);
            setBounds(getFRAME_WIDTH()-200,getFRAME_HEIGHT() -100 , 100, 50 );
            
                
        }
    }
