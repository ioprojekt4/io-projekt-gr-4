package startpackage;

import course.Course;
import maingui.MainGUI;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author Daniel
 */


public class ChoosePanel extends JFrame
{
    ArrayList<Course> coursesList;
    ArrayList<String> contentArray;
    String viewType;
    JPanel panel;
    
    ChoosePanel(ArrayList c , String viewType)
    {
        super("Wybierz element");
        panel = new JPanel();
        setContentPane(panel);
        getContentPane().setLayout(new GridLayout(5,5));
        coursesList = new ArrayList<>();
        coursesList = c;
        contentArray = new ArrayList<>();
        this.viewType = viewType;
        
        setTitle("Rozkład zajęć");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        if(viewType.contains("Nauczyciel")) //wybor odpowiedniego widoku wyboru dla konkretnego obiektu
            showTeachersToSelect();
        else
            showHallsToSelect();
        
        pack();
        setVisible(true);
        
    }

    private void showTeachersToSelect() 
    {
        String content;
        
       for(Course c : coursesList)
       {
            content = c.getCourseInstructor();
            
            if(!contentArray.contains(content))
                contentArray.add(content);
       }
       
       for(String s : contentArray)
       {
           JButton button = new JButton(s);
           System.out.println("button" +s);
            button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                MainGUI CourseGUI = new MainGUI("Rozkład zajęć dla nauczyciela: " + s , "Nauczyciel" , coursesList , s);
                setVisible(false);
                dispose();
            }
            
        });
            panel.add(button);
       }
    }

    private void showHallsToSelect() {
       String content;
        
       for(Course c : coursesList)
       {
            content = c.getWhere();
            
            if(!contentArray.contains(content))
                contentArray.add(content);
       }
       
       for(String s : contentArray)
       {
           JButton button = new JButton(s);
            button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                MainGUI CourseGUI = new MainGUI("Rozkład zajęć dla sali :" + s , "Sala" , coursesList , s);
                setVisible(false);
                dispose();
            }
        });
            panel.add(button);
       }
    }
}
