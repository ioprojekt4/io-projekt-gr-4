
package course;

import maingui.MainGUI;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Daniel
 */
public class ChangeCoursePanel
{
    JTextField timeStart;
    JTextField day;
    JTextField courseInstructor;
    JTextField where;
    JButton submit;
    DrawCourse course;
    MainGUI mainGUI ;

    public ChangeCoursePanel(DrawCourse c , int pozx , int pozy , MainGUI mainGui){
        course =c;
        mainGUI = mainGui;
        JFrame main = new JFrame("Panel zmian");
        timeStart = new JTextField(10);
        day = new JTextField(10);
        courseInstructor = new JTextField(10);
        where = new JTextField(10);
        
        main.setLocation(pozx, pozy);

        JPanel gui = new JPanel(new BorderLayout(3,3));
        gui.setBorder(new EmptyBorder(5,5,5,5));
        main.setContentPane(gui);

        JPanel labels = new JPanel(new GridLayout(0,1));
        JPanel controls = new JPanel(new GridLayout(0,1));
        gui.add(labels, BorderLayout.WEST);
        gui.add(controls, BorderLayout.CENTER);
        gui.add(new JLabel("WPROWADŹ ZMIANY"), BorderLayout.NORTH);
        
        labels.add(new JLabel("Prowadzacy:"));
        controls.add(new JLabel(c.getCourse().getCourseInstructor())); 
        labels.add(new JLabel("Sala i budynek:"));
        controls.add(new JLabel(c.getCourse().getWhere()));
        labels.add(new JLabel("Godzina rozpoczęcia: "));
        controls.add(timeStart);
        labels.add(new JLabel("Dzień: "));
        controls.add(day);
        labels.add(new JLabel("Nauczyciel: "));
        controls.add(courseInstructor); 
        labels.add(new JLabel("Sala i budynek: "));
        controls.add(where);
        submit = new JButton("Zmiana");
        
        submit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){

                
            if(checkCollisions()) //funkcja sprawdzajaca kolizje w zmianie struktury planu zajec 
            {
                JOptionPane.showMessageDialog(mainGUI.getFrame(),"Nastąpiła kolizja podczas zmiany struktury planu zajęć","Błąd",JOptionPane.ERROR_MESSAGE);
                return ;
            }
            
                try 
                {
                    mainGui.writeToFile();
                } 
                catch (IOException ex) 
                {
                    JOptionPane.showMessageDialog(mainGUI.getFrame(),"Blad podczas zapisu do pliku , nie moge kontunuowac , exit ","Błąd",JOptionPane.ERROR_MESSAGE);
                    System.out.println("Blad podczas zapisu do pliku , nie moge kontunuowac , exit ");
                    System.exit(0);
                }
                
            mainGui.getFrame().repaint();
            }

            private boolean checkCollisions() { 
                
                    String newTime ;
                    String newDay ;
                    String newInstructor ;
                    String newWhere;
                
                    newTime = timeStart.getText();
                    newDay = day.getText();
                    newInstructor = courseInstructor.getText();
                    newWhere = where.getText();
                    
                    if(!checkIfTimeIsFine(newTime , Integer.parseInt(course.getCourse().getDuration())))
                    {
                        JOptionPane.showMessageDialog(mainGUI.getFrame(),"Zły format godzinowy , wprowadź poprawny !","Błąd",JOptionPane.ERROR_MESSAGE);
                        return true;
                    }
                    
                    if(newInstructor.isEmpty() || newTime.isEmpty() || newDay.isEmpty() || newWhere.isEmpty()) 
                    {
                        JOptionPane.showMessageDialog(mainGUI.getFrame(),"Wypełij wszystkie pola","Błąd",JOptionPane.INFORMATION_MESSAGE);
                        return true;
                    } 
  
                for(Course c : mainGUI.getCoursesList())
                    if((c.compareTo(course.getCourse())) != 0)
                        if((c.getDay().contains(newDay) && checkTimeCollisions(newTime , c)))
                            return true;
                
                course.getCourse().setTimeStart(newTime); 
                course.getCourse().setDay(newDay);
                course.getCourse().setCourseInstructor(newInstructor);
                course.getCourse().setWhere(newWhere);
                course.checkIfChanged();

                return false;
            }

            private boolean checkTimeCollisions(String newTime , Course comparedObject) 
            {
                int newDuration = Integer.parseInt(course.getCourse().getDuration());
                int oldDuration = Integer.parseInt(comparedObject.getDuration());
                
                
                int newTimeInMinutes = parseStringToIntMinutes(newTime);
                int oldTimeInMinutes = parseStringToIntMinutes(comparedObject.getTimeStart());
                
                if((newTimeInMinutes <= oldTimeInMinutes + oldDuration) && (newTimeInMinutes + newDuration >= oldTimeInMinutes))
                {
                    return true;
                }
                return false;
            }

            private int parseStringToIntMinutes(String newTime) {
                int sum =0;
                
                Scanner read = new Scanner (newTime);
                read.useDelimiter(":");
                
                sum+=Integer.parseInt(read.next()) * 60;
                sum+=Integer.parseInt(read.next());
                
                return sum;
                
            }

            private boolean checkIfTimeIsFine(String newTime , int duration) {
                
                Scanner read = new Scanner (newTime);
                read.useDelimiter(":");
                int hour = Integer.parseInt(read.next());
                int minutes = Integer.parseInt(read.next());
                
                int duration_hour = duration/60;
                int duration_minutes = duration%60;
                
                if(hour < 8 || hour > 20 || minutes < 0 || minutes >59 ||(hour == 20 && duration > 60) || (hour == 20 && (duration + minutes > 60)))
                    return false;
                
                if(hour + duration_hour > 21)
                    return false;
                
                return true;
            }
        });

        gui.add(submit, BorderLayout.SOUTH);
        main.pack();
        main.setVisible(true);

    }
    
    
    public DrawCourse getCourse() {
        return course;
    }

    public void setCourse(DrawCourse course) {
        this.course = course;
    }
}
