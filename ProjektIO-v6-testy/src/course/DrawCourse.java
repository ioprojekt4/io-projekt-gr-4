/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course;

import maingui.MainGUI;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.util.Scanner;
import static maingui.MainGUI.*;

/**
 *
 * @author Daniel
 */
public class DrawCourse 
{

    private Rectangle rect = new Rectangle();
    private Point p = new Point(); 
    private Point pointLastReleased = new Point();
    private Color color;
    private int objectWidth;
    private int objectHeight;
    private Course course;
    private boolean focus = false;
    private boolean lastChoosed = false;
    private static final int NUMBER_OF_GROUPS = 5;
    private static final int NUMBER_OF_GROUPS_LAB = NUMBER_OF_GROUPS * 2;
    String objViewType;

    int cellWidth = MainGUI.getWIDTH()/6;
    int cellHeight = 10;
    int minutesInCell = 15;
    
    //uzuplenianie obiektu rysujacego konkretne zajecia
    public DrawCourse(Course c)
    { 
        course = c;
        
        
        objectHeight = (Integer.valueOf(course.getDuration())/minutesInCell) * cellHeight;
        
        if(course.getCoursesType().contains("W"))
        {
            objectWidth = getREST_COLUMN_WIDTH();
            color = new Color(146,255,183);
        }
        else if(course.getCoursesType().contains("CA"))
        {
            objectWidth = MainGUI.getWIDTH()/(6*NUMBER_OF_GROUPS);
            color = new Color(238,175,30);
        }
        else if(course.getCoursesType().contains("CL"))
        {
            objectWidth = MainGUI.getWIDTH()/(6*NUMBER_OF_GROUPS_LAB);
            color = new Color(168,243,218);
        }
        else if(course.getCoursesType().contains("P"))
        {
            objectWidth = MainGUI.getWIDTH()/(6*NUMBER_OF_GROUPS);
            color = Color.yellow;
        }       
        
        
        checkIfChanged();
        
        
    }
    
   public DrawCourse(Course c , String objectViewType)
    {
        course = c;
        objViewType = objectViewType;
        
        objectHeight = (Integer.valueOf(course.getDuration())/minutesInCell) * cellHeight;

        objectWidth = getREST_COLUMN_WIDTH();
        color = new Color(238,175,30);     

        checkIfChanged();   
    }
    
    public void draw(Graphics2D g2){
        
        int arcx=10;
        int arcy = arcx;    
        
        
        float thickness = 3f;
        Stroke oldStroke = g2.getStroke();
        g2.setStroke(new BasicStroke(thickness));
        
        g2.setColor(Color.black);
        g2.drawRoundRect((int)p.getX(), (int)p.getY(),objectWidth, objectHeight,arcx,arcy);
        
        g2.setColor(color);
        g2.fillRoundRect((int)p.getX(), (int)p.getY(),objectWidth, objectHeight,arcx,arcy);
        g2.setColor(Color.black);
        
        
        
        //rysowanie opisow przedmiotow
        g2.setFont(new Font("Arial", Font.PLAIN, 9));
        g2.drawString(course.getName(), (int)p.getX(), (int)p.getY() + objectHeight/4);
        g2.drawString(course.getWhere(), (int)p.getX(), (int)p.getY() + objectHeight/4 + 10);
        g2.drawString(course.getCoursesType(), (int)p.getX(), (int)p.getY() + objectHeight/4 + 20);
        
        g2.setStroke(oldStroke);
    }
    public boolean changeLocation(int pozx,int pozy){
        if(pozx > getTABLE_WIDTH() + getFIRST_COLUMN_WIDTH() - objectWidth || pozx < getFIRST_COLUMN_WIDTH() || pozy > getTABLE_HEIGHT() + 90 - objectHeight || pozy < 90)
        {
            System.out.println("Przeciaganie obiektu poza plan zajęć! mouseX,Y("+pozx+","+pozy+")");
            return false;
        }
        
        p.setLocation(pozx, pozy);
        rect.setLocation(p);
        
        return true;
    }
    
    public int parseFromTime(String s)
    {
        int value=90;
        
        Scanner read = new Scanner (s);
        read.useDelimiter(":");
        //sczytywanie wartosci z course.timeStart (podział na dwie wartosci , przed ":" i po ":" )
        int tempx = Integer.valueOf(read.next());   //8 => 50px;
        tempx -=8;          // dla godziny 20 bedzie 20 - 8 = 12;
        tempx*=40;
        value+=tempx;
        int tempy = Integer.valueOf(read.next());  // :00 => 
        
        
        value += (2*tempy) / 3;       
        
        return value;
    }
    
    public void setToColumnByDay()
    {
        if(course.getDay().contains("Poniedziałek"))
            p.x = getFIRST_COLUMN_WIDTH();
        else if(course.getDay().contains("Wtorek"))
            p.x = getREST_COLUMN_WIDTH()+getFIRST_COLUMN_WIDTH();   
        else if(course.getDay().contains("Środa"))
            p.x = getREST_COLUMN_WIDTH()*2+getFIRST_COLUMN_WIDTH();
        else if(course.getDay().contains("Czwartek"))
            p.x = getREST_COLUMN_WIDTH()*3+getFIRST_COLUMN_WIDTH();
        else if(course.getDay().contains("Piątek"))
            p.x = getREST_COLUMN_WIDTH()*4+getFIRST_COLUMN_WIDTH();
        
        if(course.getGroupNumber()!=0 && objViewType == null)
            p.x += objectWidth* (course.getGroupNumber()-1); 
            
            
        //ponowny nadpis rect pod elementem moim (do drag and dropa)
        rect.setLocation(p);
    }
    
    public boolean setToRightColumn(int x)
    {
        if(x > getFIRST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH() + getFIRST_COLUMN_WIDTH()) // monday
        {
            course.setDay("Poniedziałek");
        }     
        else if(x > getFIRST_COLUMN_WIDTH() + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*2 + getFIRST_COLUMN_WIDTH()) // tuesday
        {
            course.setDay("Wtorek");             
        }
 
        else if(x > getFIRST_COLUMN_WIDTH()*2 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*3 + getFIRST_COLUMN_WIDTH())
        {
            course.setDay("Środa"); 
        }
             
        else if(x > getFIRST_COLUMN_WIDTH()*3 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*4 + getFIRST_COLUMN_WIDTH())
        {
             course.setDay("Czwartek");  
        }
           
        else if(x > getFIRST_COLUMN_WIDTH()*4 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*5 + getFIRST_COLUMN_WIDTH())
        {
            course.setDay("Piątek"); 
        }
        
        setToColumnByDay();
        
        return true;
    }
    
    public int getXBeforeMoveObject(int x , DrawCourse course)

    {
        int additionalWidth=0;
        if(course.getCourse().getGroupNumber()!=0)
        {
            additionalWidth+=objectWidth* (course.getCourse().getGroupNumber()-1);
        }
            
        
        if(x > getFIRST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH() + getFIRST_COLUMN_WIDTH()) // monday
        {
            if(course.getCourse().getGroupNumber()!=0)
                return getFIRST_COLUMN_WIDTH() + additionalWidth;
            else
                return getFIRST_COLUMN_WIDTH();
        }     
        else if(x > getFIRST_COLUMN_WIDTH() + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*2 + getFIRST_COLUMN_WIDTH()) // tuesday
        {
            if(course.getCourse().getGroupNumber()!=0)
                return getREST_COLUMN_WIDTH()+getFIRST_COLUMN_WIDTH() + additionalWidth;
            else
                return getREST_COLUMN_WIDTH()+getFIRST_COLUMN_WIDTH();
        }
 
        else if(x > getFIRST_COLUMN_WIDTH()*2 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*3 + getFIRST_COLUMN_WIDTH())
        {
            if(course.getCourse().getGroupNumber()!=0)
                return getREST_COLUMN_WIDTH()*2+getFIRST_COLUMN_WIDTH() + additionalWidth;
            else
                return getREST_COLUMN_WIDTH()*2+getFIRST_COLUMN_WIDTH();
        }
             
        else if(x > getFIRST_COLUMN_WIDTH()*3 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*4 + getFIRST_COLUMN_WIDTH())
        {
            if(course.getCourse().getGroupNumber()!=0)
                return getREST_COLUMN_WIDTH()*3+getFIRST_COLUMN_WIDTH() + additionalWidth;
            else
                return getREST_COLUMN_WIDTH()*3+getFIRST_COLUMN_WIDTH();
        }
           
        else if(x > getFIRST_COLUMN_WIDTH()*4 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*5 + getFIRST_COLUMN_WIDTH())
        {
            if(course.getCourse().getGroupNumber()!=0)
                return getREST_COLUMN_WIDTH()*4+getFIRST_COLUMN_WIDTH() + additionalWidth;
            else
                return getREST_COLUMN_WIDTH()*4+getFIRST_COLUMN_WIDTH();
        }
        
        return 0;
    }
    
    public Rectangle getRect() {
        return rect;
    }

    public Point getP() {
        return p;
    }

    public Color getColor() {
        return color;
    }

    public int getObjectWidth() {
        return objectWidth;
    }

    public int getObjectHeight() {
        return objectHeight;
    }

    public Course getCourse() {
        return course;
    }
    
    public boolean isFocus() {
        return focus;
    }

    public void setFocus(boolean focus) {
        this.focus = focus;
    }
    
    public boolean isLastChoosed() {
        return lastChoosed;
    }

    public void setLastChoosed(boolean lastChoosed) {
        this.lastChoosed = lastChoosed;
    }
    public void setToRightRow(int pozy)
    {

        
        int minutes=0;
        
       minutes += ((pozy-90)/10)*15;
       
        int hours_test = minutes / 60; //since both are ints, you get an int
        int minutes_test = minutes % 60;
        
        if(minutes_test == 0)
        {
            System.out.println( hours_test + 8 + ":"+ minutes_test + "0");
            course.setTimeStart(hours_test + 8 + ":"+ minutes_test + "0");
        }                
         else
        {
            System.out.println( hours_test + 8 + ":"+ minutes_test); 
            course.setTimeStart(hours_test + 8 + ":"+ minutes_test);
        }                           
    }

    @Override
    public String toString() {
        return "DrawCourse{" + "rect=" + rect + ", p=" + p + ", color=" + color + ", width=" + objectWidth + ", height=" + objectHeight + '}';
    }

    private void setToRowByTimeStart() {
            p.y = parseFromTime(course.getTimeStart());
    }

    public void checkIfChanged() {
        setToColumnByDay();
        setToRowByTimeStart();
        rect.setBounds(p.x, p.y, objectWidth, objectHeight);
    }
    
    public Rectangle tryRectangle(int x , int y)
    {
        Rectangle tempRect = new Rectangle();
        tempRect.setBounds(x,y, objectWidth, objectHeight);
        return tempRect;
    }
    
    
    public Point getPointLastReleased() {
        return pointLastReleased;
    }

    public void setPointLastReleased(Point pointLastReleased) {
        this.pointLastReleased = pointLastReleased;
    }
}