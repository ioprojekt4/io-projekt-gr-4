package projektio;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import static projektio.MainGUI.*;

/**
 *
 * @author Daniel
 */
public class MainGUI extends JFrame
{
    private static final String FILE_WITH_COURSES = "courses.txt";
    private ArrayList<Course> coursesList= new ArrayList<>();
    private static final int FRAME_WIDTH = 1300;
    private static final int FRAME_HEIGHT = 730;
    private static final int FIRST_COLUMN_WIDTH = 1300/(7*2);
    private static final int REST_COLUMN_WIDTH = 1300/6;
    private static final int ROW_HEIGHT = 40;
    private static final int TABLE_WIDTH = 1080;
    private static final int TABLE_HEIGHT = 520;


    private tablePane panel= new tablePane(null);
    private InvisiblePanel invisiblePanel ;

    
    public static void main(String [] args)
    {
        SwingUtilities.invokeLater(()->
        {
            MainGUI myApp = new MainGUI();
            myApp.setVisible(true);
        }
        
        );
        
    }
    
    public MainGUI()
    {
        loadFile(FILE_WITH_COURSES);
        initGUI();
    }

    private void loadFile(String file) 
    {
        try 
        {
            readDataToObjects(FILE_WITH_COURSES);
            
            for(Course c : coursesList)
                System.out.println(c);
        } 
        catch (IOException ex) 
        {
            System.out.println("Nie udało się załadować pliku , exit");
            System.exit(0);
        }
        catch(ParseException ex)
        {
            System.out.println("Parse exception , exit");
            System.exit(0);
        }
    }
    
    public static int countLines(String filename) throws IOException 
    {

        InputStream is = new BufferedInputStream(new FileInputStream(filename));
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count-1;
        } finally {
            is.close();
        }

    }
    
    public void readDataToObjects(String file) throws FileNotFoundException, ParseException
    {
            Scanner read = new Scanner (new File(file));
            read.useDelimiter(",");
            Course c1 = null;
            String name,where,courseInstructor,duration,coursesType,timeStart,day;
            
       while(read.hasNext())
       {
           name = read.next();
           where = read.next();
           courseInstructor = read.next();
           day = read.next();
           timeStart= read.next(); 

           
           duration = read.next();
           coursesType = read.next();
           
           c1 = new Course(name,where,courseInstructor,day,timeStart,duration,coursesType);
           coursesList.add(c1);
       }
       read.close();
    }  

    private void initGUI() 
    {
        setTitle("Rozkład zajęć");
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        //panel.setBackground(Color.white);
        
        invisiblePanel = new InvisiblePanel(coursesList);
        
        add(invisiblePanel);
        add(panel);
        
            
    }
    

    public static int getWIDTH() {
        return FRAME_WIDTH;
    }

    public static int getHEIGHT() {
        return FRAME_HEIGHT;
    }
    
    public static int getFIRST_COLUMN_WIDTH() {
        return FIRST_COLUMN_WIDTH;
    }

    public static int getREST_COLUMN_WIDTH() {
        return REST_COLUMN_WIDTH;
    }

    public static int getROW_HEIGHT() {
        return ROW_HEIGHT;
    }
    
    public static int getTABLE_WIDTH() {
        return TABLE_WIDTH;
    }

    public static int getTABLE_HEIGHT() {
        return TABLE_HEIGHT;
    } 

}

class tablePane extends JPanel
{
    private ArrayList<String> tableTitleHours = new ArrayList<>();
    private ArrayList<String> tableTitles = new ArrayList<>();
    
    public tablePane(LayoutManager lm) 
    {
        super(lm);
        setBounds(0,0,WIDTH,HEIGHT);
        initTableTitles();
    }

    
    
    
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        draw(g);
    }
    
    public void draw(Graphics g) 
    {
        
        Graphics2D g2 = (Graphics2D) g;
        
        float thickness = 1.5f;
        Stroke oldStroke = g2.getStroke();
        g2.setStroke(new BasicStroke(thickness));
        g.setColor(Color.black);
        
        //rysujemy tabele przy zadanych warunkach rect(x , y , width , height)
         
        //rysujemy nagłówki tabeli(pionowo - godziny)
        for(int i=50 , k=0; i<MainGUI.getHEIGHT() && k<14 ; i+=40 , k++)
        {
                g2.drawRect(0,i,getFIRST_COLUMN_WIDTH(), 40);
        }
        //rysujemy nagłówki poziomo (od - do oraz dni tygodnia )
            for(int j=getFIRST_COLUMN_WIDTH(); j< MainGUI.getWIDTH()-300; j+=getREST_COLUMN_WIDTH())
                g2.drawRect(j,50,getREST_COLUMN_WIDTH(), 40);
        
        for(int i=90 , k=0; i<MainGUI.getHEIGHT() && k<12 * 4 + 1 ;k++ , i+=10)
            for(int j=getFIRST_COLUMN_WIDTH() ; j< MainGUI.getWIDTH()-300; j+=getREST_COLUMN_WIDTH())
            {
                 g2.drawRect(j,i,getREST_COLUMN_WIDTH(), 40);
            }
        
        // rysujemy uwagi do planu zajec
        g2.setFont(new Font("TimesRoman", Font.BOLD, 25));
        g2.drawString("Uwagi", MainGUI.getWIDTH() - 100, 100);
        
        //wyklad
        g2.setFont(new Font("TimesRoman", Font.PLAIN, 10));
        g2.setColor(new Color(146,255,183));
        g2.fillRect(MainGUI.getWIDTH() - 120,130,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("wykład" ,MainGUI.getWIDTH() - 70, 145 );
        
        //cwiczenia
        g2.setColor(new Color(238,175,30));
        g2.fillRect(MainGUI.getWIDTH() - 120,160,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("cwiczenia" ,MainGUI.getWIDTH() - 70, 175 );
        
        //lab
        g2.setColor(new Color(168,243,218));
        g2.fillRect(MainGUI.getWIDTH() - 120,190,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("laboratorium" ,MainGUI.getWIDTH() - 70, 205 ); 
        
        //projekt
        g2.setColor(Color.yellow);
        g2.fillRect(MainGUI.getWIDTH() - 120,220,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("projekt" ,MainGUI.getWIDTH() - 70, 235 ); 
        
        //seminarium
        g2.setColor(new Color(208,117,202));
        g2.fillRect(MainGUI.getWIDTH() - 120,250,50,20);
        g2.setColor(Color.BLACK);
        g2.drawString("seminarium" ,MainGUI.getWIDTH() - 70, 265 ); 
        
        
        g2.setStroke(oldStroke);
        
        fillTableHeaders(g2);
        

    }
    
    private void fillTableHeaders(Graphics2D g2)
    {
        //tytuł czerwony
        g2.setColor(Color.red);
        g2.setFont(new Font("TimesRoman", Font.BOLD, 25));
        g2.drawString(tableTitles.get(0), (MainGUI.getWIDTH()/2) - 350, 25);
        
        g2.setColor(Color.black);
        g2.setFont(new Font("TimesRoman", Font.PLAIN, 20));
        
        //wpisywanie do nagłówkow godzin odpowiednich stringow (pionowo godziny)
        for(int i=100 , k=0;k<tableTitleHours.size() ; i+=40 , k++)
        {
            g2.drawString(tableTitleHours.get(k), 15, i+25);
        }
        
        g2.drawString(tableTitles.get(1), 10, 85);
        
        for(int i=MainGUI.getWIDTH()/6 - 50 , k=2; k<tableTitles.size(); k++  , i+=MainGUI.getWIDTH()/6)
        {
            g2.drawString(tableTitles.get(k), i, 80);
        }
        
    }

    private void initTableTitles() {
       int k;
       for(int i=8;i<=20;i++){
           k=i+1;
           tableTitleHours.add(i + "-" + k + "");
       }
       
       tableTitles.add("RAMOWY plan zajęć IS sem 5 I st    semestr zimowy 2016/2017");
       tableTitles.add("od - do");
       tableTitles.add("Poniedziałek");
       tableTitles.add("Wtorek");
       tableTitles.add("Środa");
       tableTitles.add("Czwartek");
       tableTitles.add("Piątek");
           
    }
    
}

    class InvisiblePanel extends JPanel {
        private ArrayList<DrawCourse> drawCourse = new ArrayList<>();      
        private MouseAdapter mouseListener =
        new MouseAdapter() {
            
            int pozx,pozy;
            @Override
            public void mousePressed(MouseEvent me) {

                //System.out.println(me.getX() + "\t" + me.getY());
   
            for(DrawCourse c : drawCourse)
               if(c.isFocus())
               {
                   pozx = c.getP().x;
                   pozy = c.getP().y;
               }
                
            for(DrawCourse c : drawCourse)
               if(c.getRect().contains(me.getX(),me.getY())){
                   c.setFocus(true);
                   repaint();
                }
                    
            }

            @Override
            public void mouseReleased(MouseEvent e) {

               
            for(DrawCourse c : drawCourse)
               if(c.isFocus())
                {
                  c.setToRightColumn(e.getX());
                  c.setFocus(false);
                  repaint();   
                }
            }
            @Override
            public void mouseMoved(MouseEvent me) {
            }

            @Override
            public void mouseDragged(MouseEvent me) {

            for(DrawCourse c : drawCourse)
               if(c.isFocus() && c.changeLocation(me.getX() - c.getObjectWidth()/2,me.getY() - c.getObjectHeight()/2))
               {
                   repaint();                 
               }
               else if(c.isFocus())
               {
                   System.out.println(" ELSE IF (POZA TABELA) , pozx =" + pozx + " pozy=" + pozy);
                   c.changeLocation(pozx,pozy);
                   repaint();
               }
                  
            }
        };
       
        



        public InvisiblePanel(ArrayList<Course> courseArray) {
            // drawing should be in blue
            setForeground(Color.blue);
            // background should be black, except it's not opaque, so 
            // background will not be drawn
            setBackground(Color.black);
            // set opaque to false - background not drawn
            setOpaque(false);
            setBounds(0, 0, MainGUI.getWIDTH(),MainGUI.getHEIGHT());
            
            addMouseListener(mouseListener);
            addMouseMotionListener(mouseListener);
            
            for(Course c : courseArray)
                drawCourse.add(new DrawCourse(c));
        }

        // This is Swing, so override paint*Component* - not paint
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            
            Graphics2D g2 = (Graphics2D) g;
            
            for(DrawCourse c : drawCourse)
            {
                c.draw(g2);
            }
                
            
        }
        
    public ArrayList<DrawCourse> getDrawCourse() {
        return drawCourse;
    }


        
       
        
    }
