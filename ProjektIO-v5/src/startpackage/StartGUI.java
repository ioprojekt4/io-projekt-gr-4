package startpackage;

import course.Course;
import maingui.MainGUI;
import static maingui.MainGUI.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Daniel
 */
public class StartGUI extends JFrame
{
    private static final String FILE_WITH_COURSES = "courses.txt";
    private ArrayList<Course> coursesList;
    private JPanel panel;
    private JButton bCourse;
    private JButton bHall;
    private JButton bTeacher;
    private JButton bExit;
    
    public StartGUI()
    {
        super("Wybor widoku planu zajęć");
        initGUI();
        
    }
    
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(()->
        {
            StartGUI startGUI = new StartGUI();
        }
        
        );
    }
    
    private void initGUI() 
    {        
        panel = new JPanel();
        coursesList = new ArrayList<>();
        
        setTitle("Rozkład zajęć");
        setSize(getFRAME_WIDTH() / 2, 100);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        bCourse = new JButton("Wybierz rozkład dla studentów");
        bHall = new JButton("Wybierz rozkład dla sal");
        bTeacher = new JButton("Wybierz rozkład dla nauczycieli");
        bExit = new JButton("Wyjscie"); 
        
        loadFile(FILE_WITH_COURSES);
        
        bCourse.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                MainGUI CourseGUI = new MainGUI("Rozkład dla studentów" , "Student" , coursesList); // tutaj nie potrzeba panelu wyboru
                setVisible(false);
                dispose();
            }
        });

        bTeacher.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                ChoosePanel choosePanel = new ChoosePanel(coursesList , "Nauczyciel");
                setVisible(false);
                dispose();
            }
        });
        
        bHall.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                ChoosePanel choosePanel = new ChoosePanel(coursesList , "Sala"); 
                setVisible(false);
                dispose();
            }
        });
        
        bExit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                setVisible(false);
                dispose();
            }
        });
        
        
        panel.add(bCourse);
        panel.add(bHall);
        panel.add(bTeacher);
        panel.add(bExit, BorderLayout.SOUTH);
        
        add(panel);
        
        setVisible(true);
            
    }
    
    private void loadFile(String file) 
    {
        try 
        {
            readDataToObjects(FILE_WITH_COURSES);
        } 
        catch (IOException ex) 
        {
            JOptionPane.showMessageDialog(this,"Nie udało się załadować pliku , exit","Błąd krytyczny",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        catch(ParseException ex)
        {
            JOptionPane.showMessageDialog(this,"Parse exception , exit","Błąd krytyczny",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }
    
    public void readDataToObjects(String file) throws FileNotFoundException, ParseException
    {
            Scanner read = new Scanner (new File(file));
            read.useDelimiter(",");
            Course c1 = null;
            String name,where,courseInstructor,duration,coursesType,timeStart,day;
            
       while(read.hasNext())
       {
           name = read.next();
           where = read.next();
           courseInstructor = read.next();
           day = read.next();
           timeStart= read.next(); 

           
           duration = read.next();
           coursesType = read.next();
           
           c1 = new Course(name,where,courseInstructor,day,timeStart,duration,coursesType);
           coursesList.add(c1);
       }
       read.close();
    }
}
