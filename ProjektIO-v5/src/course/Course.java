/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package course;

import java.text.ParseException;
import java.util.Scanner;

/**
 *
 * @author Daniel
 */
public class Course implements Comparable<Course>{

    private String name;
    private String where;
    private String courseInstructor;
    private String day;
    private String timeStart;
    private String duration;
    private String coursesType;

    public Course(String name, String where, String courseInstructor,String day, String timeStart, String duration, String coursesType) {
        this.where = where;
        this.day = day;
        this.timeStart = timeStart;
        this.duration = duration;
        this.name = name;
        this.courseInstructor = courseInstructor;
        this.coursesType = coursesType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseInstructor() {
        return courseInstructor;
    }

    public void setCourseInstructor(String courseInstructor) {
        this.courseInstructor = courseInstructor;
    }

    public String getCoursesType() {
        return coursesType;
    }

    public void setCoursesType(String coursesType) {
        this.coursesType = coursesType;
    }
    
    
    public int getGroupNumber()
    {
        
        if(coursesType.contains("W"))
            return 0;
        
        int res = new Scanner(coursesType).useDelimiter("\\D+").nextInt();
        
        System.out.println("Numer grupy=" + res);
        
        return res;     
    }
    
    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }


    public String getTimeStart(){
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
    
    
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "Course{" + "name=" + name + ", where=" + where + ", courseInstructor=" + courseInstructor + ", day=" + day + ", timeStart=" + timeStart + ", duration=" + duration + ", coursesType=" + coursesType + '}';
    }

    
    /*
    private String name;
    private String where;
    private String courseInstructor;
    private String day;
    private String timeStart;
    private String duration;
    private String coursesType;
    */
    
    @Override
    public int compareTo(Course t) 
    {
        if(t.name.equals(name) && t.where.equals(where) && t.courseInstructor.equals(courseInstructor) && t.day.equals(day) && t.timeStart.equals(timeStart) && t.duration.equals(duration) && t.coursesType.equals(coursesType))
                return 0;
        else
            return 1;
    }
    

}
