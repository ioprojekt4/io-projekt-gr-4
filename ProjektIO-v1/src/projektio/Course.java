/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Daniel
 */
public class Course {

    private String name;
    private String where;
    private String courseInstructor;
    private String day;
    private String timeStart;
    private String duration;
    private String coursesType;

    public Course(String name, String where, String courseInstructor,String day, String timeStart, String duration, String coursesType) {
        this.name = name;
        this.where = where;
        this.courseInstructor = courseInstructor;
        this.day = day;
        this.timeStart = timeStart;
        this.duration = duration;
        this.coursesType = coursesType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getCourseInstructor() {
        return courseInstructor;
    }

    public void setCourseInstructor(String courseInstructor) {
        this.courseInstructor = courseInstructor;
    }

    public String getTimeStart() throws ParseException {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCoursesType() {
        return coursesType;
    }

    public void setCoursesType(String coursesType) {
        this.coursesType = coursesType;
    }
    
    
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
    

    @Override
    public String toString() {
        return "Course{" + "name=" + name + ", where=" + where + ", courseInstructor=" + courseInstructor + ", timeStart=" + timeStart + ", duration=" + duration + ", coursesType=" + coursesType + " day:" + day + '}';
    }

}
