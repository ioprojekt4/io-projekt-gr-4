/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektio;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JTextField;
import static projektio.MainGUI.*;

/**
 *
 * @author Daniel
 */
public class DrawCourse 
{

    private Rectangle rect = new Rectangle();
    private Point p = new Point(); 
    private Color color;
    private int objectWidth;
    private int objectHeight;
    private ArrayList<String> content = new ArrayList<>();
    private Course course;
    private boolean focus = false;
    private boolean lastChoosed = false;




    
    //uzuplenianie obiektu rysujacego konkretne zajecia
    DrawCourse(Course c)
    {
        int cellWidth = MainGUI.getWIDTH()/6;
        int cellHeight = 10;
        int minutesInCell = 15;
        int numberOfGroups = 5;
        
        course = c;
        
        content.add(course.getName());
        content.add(course.getWhere());
        content.add(course.getCoursesType());
        
        
        objectHeight = (Integer.valueOf(course.getDuration())/minutesInCell) * cellHeight;
        
        if(course.getCoursesType().contains("W"))
        {
            objectWidth = getREST_COLUMN_WIDTH();
            color = new Color(146,255,183);
        }
        else if(course.getCoursesType().contains("CA"))
        {
            objectWidth = MainGUI.getWIDTH()/(6*numberOfGroups);
            color = new Color(238,175,30);
        }
        else if(course.getCoursesType().contains("CL"))
        {
            objectWidth = MainGUI.getWIDTH()/(6*numberOfGroups*2);
            color = new Color(168,243,218);
        }
        else if(course.getCoursesType().contains("P"))
        {
            objectWidth = MainGUI.getWIDTH()/(6*numberOfGroups);
            color = Color.yellow;
        }       
        
        setToColumnByDay();
        
        try 
        {
            p.y = parseFromTime(course.getTimeStart());
        } 
        catch (ParseException ex) 
        {
            System.out.println("Błąd w metodzie getTimeStart() PARSE ERROR");
            ex.printStackTrace();
        }
        rect.setBounds(p.x, p.y, objectWidth, objectHeight);
    }


        
    
    public void draw(Graphics2D g2){
        
        int arcx=10;
        int arcy = arcx;    
        
        
        float thickness = 3f;
        Stroke oldStroke = g2.getStroke();
        g2.setStroke(new BasicStroke(thickness));
        
        g2.setColor(Color.black);
        g2.drawRoundRect((int)p.getX(), (int)p.getY(),objectWidth, objectHeight,arcx,arcy);
        
        g2.setColor(color);
        g2.fillRoundRect((int)p.getX(), (int)p.getY(),objectWidth, objectHeight,arcx,arcy);
        g2.setColor(Color.black);
        
        
        
        //rysowanie opisow przedmiotow
        g2.setFont(new Font("Arial", Font.PLAIN, 9));
        g2.drawString(content.get(0), (int)p.getX(), (int)p.getY() + objectHeight/4);
        g2.drawString(content.get(1), (int)p.getX(), (int)p.getY() + objectHeight/4 + 10);
        g2.drawString(content.get(2), (int)p.getX(), (int)p.getY() + objectHeight/4 + 20);
        
        g2.setStroke(oldStroke);
    }
    public boolean changeLocation(int pozx,int pozy){
        if(pozx > getTABLE_WIDTH() + getFIRST_COLUMN_WIDTH() - objectWidth || pozx < getFIRST_COLUMN_WIDTH() || pozy > getTABLE_HEIGHT() + 90 - objectHeight || pozy < 90)
        {
            System.out.println(false);
            return false;
        }
        
        p.setLocation(pozx, pozy);
        rect.setLocation(p);
        
        System.out.println(true);
        return true;
    }
    
    public int parseFromTime(String s)
    {
        int value=90;
        int x=0;
        
        Scanner read = new Scanner (s);
        read.useDelimiter(":");
        //sczytywanie wartosci z course.timeStart (podział na dwie wartosci , przed ":" i po ":" )
        int tempx = Integer.valueOf(read.next());   //8 => 50px;
        tempx -=8;          // dla godziny 20 bedzie 20 - 8 = 12;
        tempx*=40;
        value+=tempx;
        int tempy = Integer.valueOf(read.next());  // :00 => 
        
        if(tempy == 30)
            value+=20;
        
        return value;
    }
    
    public void setToColumnByDay()
    {
        if(course.getDay().contains("Monday"))
            p.x = getFIRST_COLUMN_WIDTH();
        else if(course.getDay().contains("Tuesday"))
            p.x = getREST_COLUMN_WIDTH()+getFIRST_COLUMN_WIDTH();   
        else if(course.getDay().contains("Wednesday"))
            p.x = getREST_COLUMN_WIDTH()*2+getFIRST_COLUMN_WIDTH();
        else if(course.getDay().contains("Thursday"))
            p.x = getREST_COLUMN_WIDTH()*3+getFIRST_COLUMN_WIDTH();
        else if(course.getDay().contains("Friday"))
            p.x = getREST_COLUMN_WIDTH()*4+getFIRST_COLUMN_WIDTH();
    }
    
    public boolean setToRightColumn(int x)
    {
        if(x > getFIRST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()) // monday
            course.setDay("Monday");
        else if(x > getFIRST_COLUMN_WIDTH() + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*2 + getFIRST_COLUMN_WIDTH()) // tuesday
            course.setDay("Tuesday");  
        else if(x > getFIRST_COLUMN_WIDTH()*2 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*3 + getFIRST_COLUMN_WIDTH())
            course.setDay("Wednesday");  
        else if(x > getFIRST_COLUMN_WIDTH()*3 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*4 + getFIRST_COLUMN_WIDTH())
            course.setDay("Thursday");  
        else if(x > getFIRST_COLUMN_WIDTH()*4 + getREST_COLUMN_WIDTH() && x < getREST_COLUMN_WIDTH()*5 + getFIRST_COLUMN_WIDTH())
            course.setDay("Friday");  
        return true;
    }
    
    public Rectangle getRect() {
        return rect;
    }

    public Point getP() {
        return p;
    }

    public Color getColor() {
        return color;
    }

    public int getObjectWidth() {
        return objectWidth;
    }

    public int getObjectHeight() {
        return objectHeight;
    }

    public ArrayList<String> getContent() {
        return content;
    }

    public Course getCourse() {
        return course;
    }
    
    public boolean isFocus() {
        return focus;
    }

    public void setFocus(boolean focus) {
        this.focus = focus;
    }
    
    public boolean isLastChoosed() {
        return lastChoosed;
    }

    public void setLastChoosed(boolean lastChoosed) {
        this.lastChoosed = lastChoosed;
    }

    @Override
    public String toString() {
        return "DrawCourse{" + "rect=" + rect + ", p=" + p + ", color=" + color + ", width=" + objectWidth + ", height=" + objectHeight + ", content=" + content +'}';
    }
    
    
}