
package Course;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Daniel
 */
public class ChangeCoursePanel
{
    JTextField timeStart;
    JTextField day;
    JButton submit;
    DrawCourse course;

    ChangeCoursePanel(DrawCourse c , int pozx , int pozy , MainGUI mainGui){
        course =c;
        JFrame main = new JFrame("Panel zmian");
        timeStart = new JTextField(10);
        day = new JTextField(10);
        main.setLocation(pozx, pozy);

        JPanel gui = new JPanel(new BorderLayout(3,3));
        gui.setBorder(new EmptyBorder(5,5,5,5));
        main.setContentPane(gui);

        JPanel labels = new JPanel(new GridLayout(0,1));
        JPanel controls = new JPanel(new GridLayout(0,1));
        gui.add(labels, BorderLayout.WEST);
        gui.add(controls, BorderLayout.CENTER);
        
        labels.add(new JLabel("Prowadzacy:"));
        controls.add(new JLabel(c.getCourse().getCourseInstructor())); 
        labels.add(new JLabel("Sala i budynek:"));
        controls.add(new JLabel(c.getCourse().getWhere())); 
        labels.add(new JLabel("Godzina rozpoczęcia: "));
        controls.add(timeStart);
        labels.add(new JLabel("Dzień: "));
        controls.add(day);
        submit = new JButton("Zmiana");
        
        submit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                System.out.println("action performed");
                //pozniej warunek jesli nie koliduje z innymi zajeciami , sale sa wolne i nauczyciel ma czas
            course.getCourse().setTimeStart(timeStart.getText()); 
            course.getCourse().setDay(day.getText());
            course.checkIfChanged();
                try {
                    mainGui.writeToFile();
                } catch (IOException ex) {
                    System.out.println("Blad podczas zapisu do pliku , nie moge kontunuowac , exit ");
                    System.exit(0);
                }
            mainGui.getFrame().repaint();
            }
        });

        gui.add(submit, BorderLayout.SOUTH);
        main.pack();
        main.setVisible(true);

    }
    
    
    public DrawCourse getCourse() {
        return course;
    }

    public void setCourse(DrawCourse course) {
        this.course = course;
    }
}
